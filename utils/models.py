import torch
import torch.nn as nn
import sys
sys.path.append("../")
import spdnetwork.nn as nn_spd


class ConvSPD_3_Conv_1_SPD(nn.Module):
    
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) 
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) 
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 64, kernel_size=(3,3,3), padding = 1) 
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.conv3 = nn.Conv3d(in_channels = 64,out_channels = 128, kernel_size=(3,3,3),padding = 1) 
        self.relu3 = nn.ReLU()
        self.max3 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2))
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),                                           
                                nn_spd.LogEig())        
        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 


        self.indices = torch.triu_indices(32, 32)

    def forward(self,x):
        x = self.conv1(x) 
        x = self.relu1(x) 
        x = self.max1(x) 
        x = self.conv2(x) 
        x = self.relu2(x)
        x = self.max2(x) 
        x = self.conv3(x) 
        x = self.relu3(x) 
        x = self.max3(x) 
        x = x.view(x.size(0),x.size(1), -1) 
        x = self.covariancePooling(x) 
        x = self.bimap_log(x) 

        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)

        
        return x

class ConvSPD_1_Conv_1_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 128, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de 
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,32,dtype = dtype, device = device), #(No.Mat output,No.matrices input,Size input,size 
                                nn_spd.ReEig(),                                           
                                nn_spd.LogEig())        
        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x
 
class ConvSPD_3_Conv_2_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 64, kernel_size=(3,3,3), padding = 1) #(batch,64,T/2,W,H)
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.conv3 = nn.Conv3d(in_channels = 64,out_channels = 128, kernel_size=(3,3,3),padding = 1) #(batch,128,15,25,25)
        self.relu3 = nn.ReLU()
        self.max3 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2))
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = self.conv2(x) # Output size == (batchm,64,T,W/2,H/2) 
        x = self.relu2(x)
        x = self.max2(x) # Output size == (batchm,64,T,W/4,H/4) 
        x = self.conv3(x) # Output size == (batchm,128,T,W/4,H/4) 
        x = self.relu3(x) 
        x = self.max3(x) # Output size == (batchm,128,T,W/8,H/8) 
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x

    
class ConvSPD_2_Conv_1_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 128, kernel_size=(3,3,3), padding = 1) #(batch,64,T/2,W,H)
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,32,dtype = dtype, device = device), #(No.Mat output,No.matrices input,Size input,size output)
                                nn_spd.ReEig(),                                           
                                nn_spd.LogEig())        
        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = self.conv2(x) # Output size == (batchm,64,T,W/2,H/2) 
        x = self.relu2(x)
        x = self.max2(x) # Output size == (batchm,64,T,W/4,H/4) 
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x    
    
class ConvSPD_2_Conv_2_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 128, kernel_size=(3,3,3), padding = 1) #(batch,64,T/2,W,H)
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = self.conv2(x) # Output size == (batchm,64,T,W/2,H/2) 
        x = self.relu2(x)
        x = self.max2(x) # Output size == (batchm,64,T,W/4,H/4) 
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x
    
    
    
    
    
class ConvSPD_3_Conv_3_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 64, kernel_size=(3,3,3), padding = 1) #(batch,64,T/2,W,H)
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.conv3 = nn.Conv3d(in_channels = 64,out_channels = 128, kernel_size=(3,3,3),padding = 1) #(batch,128,15,25,25)
        self.relu3 = nn.ReLU()
        self.max3 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2))
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,32,16,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        self.linear1 = nn.Linear(16**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = self.conv2(x) # Output size == (batchm,64,T,W/2,H/2) 
        x = self.relu2(x)
        x = self.max2(x) # Output size == (batchm,64,T,W/4,H/4) 
        x = self.conv3(x) # Output size == (batchm,128,T,W/4,H/4) 
        x = self.relu3(x) 
        x = self.max3(x) # Output size == (batchm,128,T,W/8,H/8) 
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x
    
class ConvSPD_2_Conv_3_SPD(nn.Module):
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        #kernel: (depth, H, W)
        #Maxpooling: (z,y,x)
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 32, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.conv2 = nn.Conv3d(in_channels = 32,out_channels = 128, kernel_size=(3,3,3), padding = 1) #(batch,64,T/2,W,H)
        self.relu2 = nn.ReLU()
        self.max2 = nn.MaxPool3d(kernel_size = (2,2,2), stride=(2,2,2)) 
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,32,16,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        self.linear1 = nn.Linear(16**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = self.conv2(x) # Output size == (batchm,64,T,W/2,H/2) 
        x = self.relu2(x)
        x = self.max2(x) # Output size == (batchm,64,T,W/4,H/4) 
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x
    
class ConvSPD_1_Conv_3_SPD(nn.Module): 
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 128, kernel_size = (3,3,3), padding = 1) #(batch,3CANALES,Numero de frames,H,W) -> (100,3,60,200,300)
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,32,16,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        self.linear1 = nn.Linear(16**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x
    
    
class ConvSPD_1_Conv_2_SPD(nn.Module):  #este
    # def __init__(self):
    def __init__(self,dtype,device,in_channels = 3):
        super(__class__,self).__init__()        
        self.conv1 = nn.Conv3d(in_channels = in_channels,out_channels = 128, kernel_size = (3,3,3), padding = 1) 
        self.relu1 = nn.ReLU()
        self.max1 = nn.MaxPool3d(kernel_size = (1,2,2), stride=(1,2,2)) #(batch,32,T,W,H)
        self.covariancePooling = nn_spd.CovPool()
        self.bimap_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.LogEig())      
        


        self.linear1 = nn.Linear(32**2,128)        
        self.linear2 = nn.Linear(128,1) 

    def forward(self,x):
        # En redes 3D pytorch espera: [S. Batch, Input channels, depth, H,W]
        # print(f'antes de CONV x type: {x.dtype} x shape {x.shape}') #(batch,3CANALES,Numero de frames,H,W) -> (batch,3,T,H,W)
        x = self.conv1(x) # Output size == (batchm,32,T,W,H)
        x = self.relu1(x) 
        x = self.max1(x) # Output size == (batchm,32,T,W/2,H/2)
        x = x.view(x.size(0),x.size(1), -1) # Output size == (batchm,128,T*W/8*H/8) 
        x = self.covariancePooling(x) # Output size == (batchm,1,128,128) 
        x = self.bimap_log(x) # Output size == (batchm,1,32,32) 
        x = x.view(x.shape[0],-1) # Output size == (batchm,32*32)
        x = self.linear1(x) # Output size == (batchm,128)
        x = self.linear2(x) # Output size == (batchm,1)
        return x