import numpy as np
import cv2
import glob
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import datetime
import pytz


def get_all_directories(folder_path, file_type = "*"):
    """
    Returns a list of directories in a folder that match the specified file type.

    Args:
        folder_path (str): The path to the folder to search in.
        file_type (str, optional): The file type to match. The default value is "*".

    Returns:
        list: A list of directory paths that match the search.
    """
    directories = glob.glob(os.path.join(folder_path, file_type))
    return directories

def bgr_to_rgb(image):
    """
    Convert an image from BGR to RGB color space.

    Args:
        image (numpy.ndarray): The image to be converted.

    Returns:
        numpy.ndarray: The converted image.
    """
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

def read_first_frame_from_video(video_path):
    """
    Reads the first frame of a video and returns it as a numpy array in RGB color space.

    Args:
        video_path (str): The path to the input video file.

    Returns:
        numpy.ndarray: The first frame of the input video in RGB color space.
    """
    try:
        cap = cv2.VideoCapture(video_path) #open the video file
        ret, frame = cap.read() # Read the first frame of the video
        # print(frame.shape)
        frame = bgr_to_rgb(frame) # Convert the frame from BGR to RGB color space
        cap.release() # Release the video capture object
        return frame
    except:
        print("Error reading video file: {}".format(video_path))
    return frame

def plot_image(image, title = "", **kwargs):
    """
    Plots an image.

    Args:
        image (numpy.ndarray): The image to be plotted.
        title (str, optional): The title of the plot. The default value is "".

    Returns:
        N
    """
    fig, axs = plt.subplots(figsize = (5,5))
    axs.imshow(image, **kwargs)
    axs.axis("off")
    axs.set_title(title)
    plt.show()
    
def plot_first_frame_videos_in_folder(folder_path):
    """
    Given a folder path containing videos in .MOV format, plots the first frame of each video.

    Args:
        folder_path (str): The path to the folder containing the videos.

    Returns:
        N
    """
    videos = sorted(get_all_directories(folder_path=folder_path, file_type="*.MOV")) # Get all .MOV files in the folder
    
    for path_video in videos:
        print(path_video.split("/")[-1]) # Print the name of the current video file
        img = read_first_frame_from_video(path_video) # Read the first frame of the current video file
        
        # Plot the first frame of the current video file
        fig, axs = plt.subplots(figsize = (5,5))
        axs.imshow(img)
        axs.axis("off")
        plt.show()
   
    
def resize_template(template, image):
    """
    Resizes the template image to match the size of the input image, if it is bigger.
    Args:
        template: Template image to be resized.
        image: Input image used as reference for resizing the template.
    Returns:
        Resized template image.
    """
    # Get the dimensions of the input image and the template
    image_height, image_width, _ = image.shape
    template_height, template_width, _ = template.shape

    # Check if the template is bigger than the input image
    if template_height > image_height or template_width > image_width:
        # Resize the template image proportionally to 80% of the input image width and 40% of its height
        new_width, new_height = int(0.7 * image_width), int(0.35 * image_height)
        resized_template = cv2.resize(template, (new_width, new_height))
        return resized_template
    
    # If the template is smaller or equal to the input image, return the original template
    return template

def detect_eyes(image, template_left_path, template_right_path):
    """
    Detects the positions of the left and right eyes in an RGB image using template matching.

    Parameters:
    -----------
    image : numpy.ndarray
        An RGB image to search for eyes.
    template_left_path : str
        The file path for the template image of the left eye.
    template_right_path : str
        The file path for the template image of the right eye.

    Returns:
    --------
    tuple
        A tuple containing two tuples, one for each eye. Each inner tuple contains the (x, y) coordinates of the
        top-left corner of the bounding box around the eye, followed by the width and height of the bounding box.
    """
    # Load the templates
    template_left = cv2.imread(template_left_path)
    template_right = cv2.imread(template_right_path)

    # Convert to grayscale
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    gray_template_left = cv2.cvtColor(template_left, cv2.COLOR_BGR2GRAY)
    gray_template_right = cv2.cvtColor(template_right, cv2.COLOR_BGR2GRAY)

    # Match right eye
    match_right = cv2.matchTemplate(gray_image, gray_template_right, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match_right)
    ex_right, ey_right, ew_right, eh_right = max_loc[0], max_loc[1], gray_template_right.shape[1], gray_template_right.shape[0]

    # Match left eye
    match_left = cv2.matchTemplate(gray_image, gray_template_left, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match_left)
    ex_left, ey_left, ew_left, eh_left = max_loc[0], max_loc[1], gray_template_left.shape[1], gray_template_left.shape[0]

    return (ex_right, ey_right, ew_right, eh_right), (ex_left, ey_left, ew_left, eh_left)

def plot_eyes_matching(image: np.ndarray, template_left_path: str, template_right_path: str) -> None:
    """
    Displays the input image along with the bounding boxes for the detected left and right eyes, based on the input templates.
    
    Args:
    - image (numpy.ndarray): An RGB image of shape (height, width, 3).
    - template_left_path (str): A string path to the image template for the left eye.
    - template_right_path (str): A string path to the image template for the right eye.
    
    Returns:
    - None: This function doesn't return anything. It displays the image with the bounding boxes for the detected eyes using matplotlib.
    
    Raises:
    - FileNotFoundError: If the input `template_left_path` or `template_right_path` cannot be found or accessed.
    
    Notes:
    - This function uses the `detect_eyes` function to obtain the eye positions based on the input templates.
    - The eye positions are displayed as rectangular bounding boxes with different colors: red for the left eye, and orange for the right eye.
    """
    # Get the eye positions based on the input templates
    (ex_right, ey_right, ew_right, eh_right), (ex_left, ey_left, ew_left, eh_left) = detect_eyes(image, template_left_path, template_right_path)
    
    # Create the plot with the input image
    fig, axs = plt.subplots(figsize=(5, 5))
    axs.imshow(image)
    
    # Add rectangular bounding boxes for the left and right eyes
    rect_right = patches.Rectangle((ex_left, ey_left), ew_left, eh_left, linewidth=2, edgecolor='magenta', facecolor='none')
    rect_left = patches.Rectangle((ex_right, ey_right), ew_right, eh_right, linewidth=2, edgecolor='cyan', facecolor='none')
    axs.add_patch(rect_right)
    axs.add_patch(rect_left)
    axs.axis("off") # Remove the axis ticks and labels
    plt.show()
    
def get_date() -> str:
    """
    Returns the current date and time in the "dd/mm/yyyy hh:mm:ss" format, 
    localized to the America/Bogota timezone.
    
    Returns:
        A string with the current date and time.
    """
    utc_now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    pst_now = utc_now.astimezone(pytz.timezone("America/Bogota"))
    fecha = pst_now.strftime("%d/%m/%Y %H:%M:%S")
    return fecha    

def get_date():
    now = datetime.datetime.now()
    return now.strftime("%y%m%d-%H%M%S")

def create_folder_with_nomenclature(folder_path: str, output_name_model: str) -> str:
    """
    Creates a new folder with a nomenclature of "000", or increments the nomenclature 
    if the folder already exists.
    
    Args:
        folder_path (str): The path of the folder to create.
        output_name_model (str): The name of the output model.
    
    Returns:
        str: The path of the created folder.
    """
    nomenclature = 0
    
    while True:
        folder_name = f"{nomenclature:04d}_{output_name_model}"
        new_folder_path = os.path.join(folder_path, folder_name)
        
        if not os.path.exists(new_folder_path):
            print(f"Creating folder: {new_folder_path}")
            os.makedirs(new_folder_path)
            break
        
        nomenclature += 1
    
    return new_folder_path, folder_name
