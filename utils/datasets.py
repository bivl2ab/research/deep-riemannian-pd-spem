import torch
import torch.utils.data as data
from torchvision.transforms import transforms

from PIL import Image
import os
import random
import logging

class VideosSPdataset(data.Dataset):
    """A Custom Dataset for loading video frames from a folder and returning them as a tensor.

    Args:
        root_dir (str): The path to the root directory containing video folders.
        names_patients (list): A list of patient names to include in the dataset.
        subsampling_step (int): The step size for subsampling the video frames.
        transform (callable, optional): A function/transform that takes in a PIL image and returns a transformed version.

    Attributes:
        root_dir (str): The path to the root directory containing video folders.
        transform (callable or None): A function/transform that takes in a PIL image and returns a transformed version.
        video_folders (list): A list of video folders in root_dir.
        subsampling_step (int): The step size for subsampling the video frames.

    """

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder,"augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients] #folder[:3] is the patient name. Ej: `P01`
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder,"augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients)            
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        """Loads the video frames from a folder and returns them as a tensor along with the video label.

        Args:
            index (int): The index of the video folder to load.

        Returns:
            video_tensor (torch.Tensor): A tensor of size (T, C, H, W) representing the video frames, 
            where T is the number of frames in the video, C is the number of channels, H is the height, 
            and W is the width.
            label (int): The label of the video, 1 if it is a 'P' video, 0 otherwise.
        """
        
        # print(f"Loading... {self.video_folders[index]}")
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []
                
        for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
            if i % self.subsampling_step == 0: # subsampling
                frame_path = os.path.join(folder_path, frame_name)
                frame = Image.open(frame_path)
                if self.transform is not None:
                    frame = self.transform(frame)
                    if augm_info == "augm1": # apply augmentation
                        frame = self.augm1(frame)                    
                frames.append(frame)
        # (T, C, H, W) -> (C, T, H, W) to match with the input shape of the models. 
        # T is the number of frames in the video, C is the number of channels, H is the height, and W is the width.                
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient

class VideosSPdataset2(data.Dataset):

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder,"augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients] #folder[:3] is the patient name. Ej: `P01`
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder,"augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients)            
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        #---
        # print(f"Loading... {self.video_folders[index]}")
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []

        for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
            if i<480 and i % self.subsampling_step == 0: # subsampling
                frame_path = os.path.join(folder_path, frame_name)
                frame = Image.open(frame_path)
                if self.transform is not None:
                    frame = self.transform(frame)
                    if augm_info == "augm1": # apply augmentation
                        frame = self.augm1(frame)                    
                frames.append(frame)
        #print(len(frames))
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient 
class VideosSPdataset3(data.Dataset):

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder,"augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients] #folder[:3] is the patient name. Ej: `P01`
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder,"augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients)            
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        #---
        # print(f"Loading... {self.video_folders[index]}")
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []

        for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
            if i>=480 and i % self.subsampling_step == 0: # subsampling
                frame_path = os.path.join(folder_path, frame_name)
                frame = Image.open(frame_path)
                if self.transform is not None:
                    frame = self.transform(frame)
                    if augm_info == "augm1": # apply augmentation
                        frame = self.augm1(frame)                    
                frames.append(frame)
        #print(len(frames))
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient
    
class VideosSPdataset4(data.Dataset):
    """A Custom Dataset for loading video frames from a folder and returning them as a tensor.

    Args:
        root_dir (str): The path to the root directory containing video folders.
        names_patients (list): A list of patient names to include in the dataset.
        subsampling_step (int): The step size for subsampling the video frames.
        transform (callable, optional): A function/transform that takes in a PIL image and returns a transformed version.

    Attributes:
        root_dir (str): The path to the root directory containing video folders.
        transform (callable or None): A function/transform that takes in a PIL image and returns a transformed version.
        video_folders (list): A list of video folders in root_dir.
        subsampling_step (int): The step size for subsampling the video frames.

    """

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder,"augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients] #folder[:3] is the patient name. Ej: `P01`
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder,"augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients)            
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        """Loads the video frames from a folder and returns them as a tensor along with the video label.

        Args:
            index (int): The index of the video folder to load.

        Returns:
            video_tensor (torch.Tensor): A tensor of size (T, C, H, W) representing the video frames, 
            where T is the number of frames in the video, C is the number of channels, H is the height, 
            and W is the width.
            label (int): The label of the video, 1 if it is a 'P' video, 0 otherwise.
        """
        
        # print(f"Loading... {self.video_folders[index]}")
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []
 
        if "ve" in folder_path:         
            for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
                if i % 5 == 0: # subsampling
                    frame_path = os.path.join(folder_path, frame_name)
                    frame = Image.open(frame_path)
                    if self.transform is not None:
                        frame = self.transform(frame)
                        if augm_info == "augm1": # apply augmentation
                            frame = self.augm1(frame) 
                    if len(frames) < 20:
                        frames.append(frame)

                    
        else:
            for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
                if i % self.subsampling_step == 0: # subsampling
                    frame_path = os.path.join(folder_path, frame_name)
                    frame = Image.open(frame_path)
                    if self.transform is not None:
                        frame = self.transform(frame)
                        if augm_info == "augm1": # apply augmentation
                            frame = self.augm1(frame)                    
                    frames.append(frame)
        # (T, C, H, W) -> (C, T, H, W) to match with the input shape of the models. 
        # T is the number of frames in the video, C is the number of channels, H is the height, and W is the width.                
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient

class VideosSPdatasetver(data.Dataset):

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder, "augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients and "ve" in folder]
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder, "augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients and "ve" in folder)
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []

        for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
            if i % self.subsampling_step == 0: # subsampling
                frame_path = os.path.join(folder_path, frame_name)
                frame = Image.open(frame_path)
                if self.transform is not None:
                    frame = self.transform(frame)
                    if augm_info == "augm1": # apply augmentation
                        frame = self.augm1(frame)                    
                frames.append(frame)
        #print(len(frames))
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient
    

    
class VideosSPdatasetho(data.Dataset):

    def __init__(self, root_dir: str, names_patients: list, data_augmentation: bool, subsampling_step: int = 1, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.subsampling_step = subsampling_step
        self.video_folders = [(folder, "augmNone") for folder in os.listdir(root_dir) if folder[:3] in names_patients and "ho" in folder]
        
        logging.info(f"Number of patient names: {len(self.video_folders)}")
        if data_augmentation == True:
            logging.info("Using Data agumentation. Augmenting with Horizontal Flip")
            self.video_folders.extend((folder, "augm1") for folder in os.listdir(root_dir) if folder[:3] in names_patients and "ho" in folder)
            self.augm1 = transforms.RandomHorizontalFlip(p=1) # Horizontal Flip with probability 1


    def __len__(self):
        """Returns the number of video folders in the dataset."""
        return len(self.video_folders)

    def __getitem__(self, index):
        name_patient, augm_info = self.video_folders[index]
        folder_path = os.path.join(self.root_dir, name_patient)
        frames = []

        for i, frame_name in enumerate(sorted(os.listdir(folder_path))):
            if i % self.subsampling_step == 0: # subsampling
                frame_path = os.path.join(folder_path, frame_name)
                frame = Image.open(frame_path)
                if self.transform is not None:
                    frame = self.transform(frame)
                    if augm_info == "augm1": # apply augmentation
                        frame = self.augm1(frame)                    
                frames.append(frame)
        #print(len(frames))
        video_tensor = torch.stack(frames).transpose(0, 1) 
        
        label = int(name_patient[0] == 'P')

        # print(f'name_patient: {name_patient}, label" {label}')
        
        name_patient = name_patient + "_" + augm_info
        return video_tensor, label, name_patient
    
def get_5kf_fold_partitions(seed):
    """
    Generate a dictionary of partitions for each fold from a list of patients, where each partition
    contains 5 patients (fold5 has 6 patients), and each partition has a minimum of 2 patients from each 
    class to be balanced (Parkinson or Control).

    Args:
        seed (int): The seed used to ensure reproducibility of the results.

    Returns:
        dict: A dictionary containing the partitions for each fold.
    """
    parkinson_patients = [f"P{index:02}" for index in range(11)]
    control_patients = [f"C{index:02}" for index in range(11)]
    all_patients = parkinson_patients + control_patients
    available_patients = all_patients.copy()

    folds_dict = {}
    for i in range(1, 5): # 4 folds de 4 pacientes
        val_set = get_balanced_5fold_subset(available_patients, random_seed=seed)
        folds_dict[f"fold_{i}_val"] = val_set
        folds_dict[f"fold_{i}_train"] = [p for p in all_patients if p not in val_set]
        available_patients = [p for p in available_patients if p not in val_set]
        seed += 1

    folds_dict["fold_5_val"] = available_patients #5to fold con 6 pacientes
    folds_dict["fold_5_train"] = [p for p in all_patients if p not in available_patients]
    return folds_dict

def get_balanced_5fold_subset(input_list, random_seed):
    """
    Returns a list of 5 unique elements randomly selected from input_list, with at least 2 elements from
    each class (Parkinson or control).
    """
    subset_found = False
    while not subset_found:
        c_count, p_count = 0, 0
        subset = []
        for element in get_unique_random_subset(input_list = input_list, 
                                                num_elements = 4,     
                                                random_seed = random_seed):
            if "C" in element:
                c_count += 1
            else:
                p_count += 1
            subset.append(element)
        if c_count >= 2 and p_count >= 2:
            subset_found = True
        else:
            random_seed += 1
    return subset

def get_unique_random_subset(input_list, num_elements, random_seed=None):
    """
    Returns a sorted list of num_elements unique elements randomly selected from input_list.
    """
    subset_found = False
    while not subset_found:  
        if random_seed is not None:
            random.seed(random_seed)
            random_subset = sorted(random.sample(input_list, num_elements)) # get a random subset of size num_element
        else:
            random_subset = sorted(random.sample(input_list, num_elements))
        if len(set(random_subset)) == num_elements:
            subset_found = True
    return random_subset

