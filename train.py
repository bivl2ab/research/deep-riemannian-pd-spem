# ------- Common libraries
import sys
sys.path.append('../')
import glob
import os
#os.environ['CUBLAS_WORKSPACE_CONFIG'] = ':4096:8'  # or ':16:8'
#os.environ['CUDNN_rRMINISTIC'] = '1'

import numpy as np
import random
import csv
from sklearn.metrics import accuracy_score
import logging
import json

# ------- Pytorch libraries
import torch
torch.set_num_threads(1) #otherwise pytorch will take all cpus
import torch.nn as nn
from torch.utils import data
from torch.utils.data import DataLoader
from torchvision.transforms import Resize
from torchvision import transforms
import torch.utils.tensorboard as tb

# ------- Others libraries
from spdnetwork.optimizers import MixOptimizer 
import utils.datasets as datasets
from utils.datasets import get_5kf_fold_partitions
import utils.models as models
from utils.utils import get_date, get_date


def seed_worker(worker_id): #this function is to set the seed for each workers
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


# Define transforms for the training data
train_transforms = transforms.Compose([
    transforms.Resize(64), #Re-scale the image keeping the original aspect ratio
    # transforms.RandomCrop(224),
    transforms.ColorJitter(brightness=0.2, contrast=0.4, saturation=0.2, hue=0.1),
    # transforms.RandomHorizontalFlip(), #Flip the image with probability = 0.5
    transforms.ToTensor(), #Convert the image to a tensor with pixels in the range [0, 1]
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) #Normalize the image with mean and standard deviation of ImageNet data
])

# Define transforms for the validation data
val_transforms = transforms.Compose([
    transforms.Resize(64), #256!!!
    # transforms.CenterCrop(224),
    transforms.ColorJitter(brightness=0.2, contrast=0.4, saturation=0.2, hue=0.1),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])
#----------------------------------------------------------------
# CONFIG MODEL PARAMETERS
#----------------------------------------------------------------
dtype = torch.double
    
PARAMETERS = {    
    "data_root_dir": "../../../../../data/Datasets/Fer/data/temporal/frames_cambio/",
    "output_root_dir": "../Att_outputs_PT/",
    "name_model": "productSPD_att",
    "name_dataset": "VideosSPdataset",  #ojito
    "data_augmentation": True,
    "step_save_checkpoints":5, #None if you don't want to save checkpoints
    "subsampling_step": 6, #  60 fps / 6 fps = 10... so Chose 6. 
    "batch_size": 4, # Batch de 32
    "epoch": 145,
    'optimizer':{
        'kind': 'Adam',
        "lr": 0.0001,    #origini 0.0001
        'lr_others': 0.00001, 
        "momentum": 0.9,
    },    
    "seed": 2000,
    "GPU_DEVICE_NUMBER": "0"
    }




# Set the seed for reproducibility
torch.manual_seed(PARAMETERS["seed"])
# torch.backends.cudnn.deterministic = True
# torch.use_deterministic_algorithms(True)
random.seed(PARAMETERS["seed"])

#----------------------------------------------------------------
# CREATE FOLDER AND NAMING MODEL
#----------------------------------------------------------------
lr_save_name = str(PARAMETERS['optimizer']['lr']).replace("0.","p")
lro_save_name = str(PARAMETERS['optimizer']['lr_others']).replace("0.","p")

PARAMETERS["output_name_model"] = f"{PARAMETERS['name_model']}_{PARAMETERS['name_dataset']}_bs-{PARAMETERS['batch_size']}_lr-{lr_save_name}_{get_date()}"
PARAMETERS["output_dir"] = f"{PARAMETERS['output_root_dir']}/models/{PARAMETERS['output_name_model']}"

print(f"Creating folder: {PARAMETERS['output_dir']}")
os.makedirs(PARAMETERS['output_dir'])

#Create the logger file
logging.basicConfig(filename = os.path.join(PARAMETERS['output_dir'],"output.log"), 
                    level=logging.INFO, 
                    format='%(asctime)s %(levelname)s: %(message)s')


#----------------------------------------------------------------
# Selecting the device
#----------------------------------------------------------------
if PARAMETERS["GPU_DEVICE_NUMBER"]:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = PARAMETERS["GPU_DEVICE_NUMBER"] #aca se pone nuemro de grafica libre

if torch.cuda.is_available():
    device = torch.device('cuda')
    logging.info(f'Using GPU: {torch.cuda.get_device_name()}')
    logging.info(f'CUDA Visible devices: {os.getenv("CUDA_VISIBLE_DEVICES")}')
else:
    device = torch.device('cpu')
    logging.info('Failed to find GPU, using CPU instead.')
       
#----------------------------------------------------------------
# Saving parameters
#----------------------------------------------------------------
json.dump(PARAMETERS, open(os.path.join(PARAMETERS["output_dir"],"parameters.json"), "w"))
    
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#                      5K - FOLD VALIDATION
#----------------------------------------------------------------------
#----------------------------------------------------------------------
    
# Create the CSV file and write the headers
with open(os.path.join(PARAMETERS['output_dir'],"trainingVal_info.csv"), 'w', newline='') as file:
    writer_csv = csv.writer(file)
    writer_csv.writerow(["Fold_index",'Training/Val','Epoch', 'Batch', 'Loss', 'names_patients', "labels", "predictions"])
    
    data_folds_dict = get_5kf_fold_partitions(seed = PARAMETERS["seed"])

    for idx_fold in range(1,6):
        logging.info(f"Starting fold {idx_fold}")
        train_names = data_folds_dict[f"fold_{idx_fold}_train"]
        val_names = data_folds_dict[f"fold_{idx_fold}_val"]

        logging.info(f"Train names: {data_folds_dict[f'fold_{idx_fold}_train']}")
        logging.info(f"Val names: {data_folds_dict[f'fold_{idx_fold}_val']}")

        train_data = getattr(datasets, PARAMETERS['name_dataset'])(
                                    root_dir = PARAMETERS["data_root_dir"], 
                                    names_patients = train_names,
                                    data_augmentation = PARAMETERS["data_augmentation"],
                                    subsampling_step =  PARAMETERS["subsampling_step"],
                                    transform = train_transforms)

        val_data = getattr(datasets, PARAMETERS['name_dataset'])(
                                    root_dir = PARAMETERS["data_root_dir"],
                                    names_patients = val_names,
                                    data_augmentation = False,
                                    subsampling_step =  PARAMETERS["subsampling_step"],
                                    transform = val_transforms)

        # Create the dataloaders and seeding the workers
        g = torch.Generator()
        g.manual_seed(PARAMETERS["seed"])

        train_loader = DataLoader(dataset = train_data, 
                                batch_size = PARAMETERS["batch_size"], 
                                worker_init_fn = seed_worker,
                                shuffle = True)
        val_loader = DataLoader(dataset = val_data, 
                                batch_size = PARAMETERS["batch_size"], 
                                worker_init_fn = seed_worker,
                                shuffle = False)    
        
        logging.info(f"Train loader size: {len(train_loader)} with batch_size: {PARAMETERS['batch_size']}")
        logging.info(f"Val loader size: {len(val_loader)} with batch_size: {PARAMETERS['batch_size']}")
        #Define the model 
        model = getattr(models, PARAMETERS['name_model'])(dtype = dtype, 
                                                    device = device)

        criterion = nn.BCEWithLogitsLoss()

        #----------------------------------------------------------------
        # OPTIMIZERS 
        #----------------------------------------------------------------
        optimizer_mapping = {
            'SGD': (torch.optim.SGD, {
                                'lr': PARAMETERS['optimizer']['lr'],
                                'lr': PARAMETERS['optimizer']['lr_others']
                                }),
            'Adam': (torch.optim.Adam, {
                                'lr': PARAMETERS['optimizer']['lr'],
                                'lr': PARAMETERS['optimizer']['lr_others']
                                }),
            'RMSprop': (torch.optim.RMSprop, {
                                'lr': PARAMETERS['optimizer']['lr'], 
                                'lr': PARAMETERS['optimizer']['lr_others'],                           
                                'momentum': PARAMETERS['optimizer']['momentum']
            })
        }
        
        optimizer_class, optimizer_args = optimizer_mapping[PARAMETERS['optimizer']['kind']]


        optimizer = MixOptimizer(model.parameters(), 
                                 optimizer = optimizer_class,
                                 **optimizer_args)

        model.to(device)
        model.type(dtype)

        # Create a SummaryWriter object            
        writer = tb.SummaryWriter(log_dir = f"{PARAMETERS['output_root_dir']}runs/fold-{idx_fold}_{PARAMETERS['output_name_model']}")
        writer.add_scalar(f"Hyperparameters/subsampling_step", PARAMETERS["subsampling_step"])
        writer.add_scalar(f"Hyperparameters/batch_size", PARAMETERS["batch_size"])
        writer.add_scalar(f"Hyperparameters/lr", PARAMETERS['optimizer']["lr"])
        writer.add_scalar(f"Hyperparameters/lr_others", PARAMETERS['optimizer']['lr_others'])
        writer.add_scalar(f"Hyperparameters/momentum", PARAMETERS['optimizer']["momentum"])

        for epoch in range(PARAMETERS["epoch"]):
            train_loss = 0.0
            
            logging.info(f"Starting Trainig Epoch {epoch}")
            for i, (inputs, labels, name_patients) in enumerate(train_loader):
                inputs, labels = inputs.to(device), labels.to(device)
                inputs = inputs.type(dtype) #convert to double
                
                optimizer.zero_grad()
                

                # with torch.backends.cudnn.flags(deterministic=True):
                outputs = model(inputs)             
                outputs = torch.squeeze(outputs,1) #remove the dimension 1     
                #labels = labels.view(-1, 1)   #att
                                         
                loss = criterion(outputs,labels.float())
                                            
                labels = labels.detach().cpu().numpy() 
                output_prob = torch.sigmoid(outputs).detach().cpu().numpy()
                name_patients = list(name_patients)
                
                # print(output_prob)
                # print(labels)

                preds= np.round(output_prob) #round to 0 or 1
                acc_score = accuracy_score(y_true = labels, y_pred = preds)        
                
                logging.info(f"labels: {labels}, probs: {output_prob}, preds: {preds}")
                logging.info(f"Epoch [{epoch}/{PARAMETERS['epoch']}] - Step [{i}/{len(train_loader)-1}] - Train Loss: {loss.item():.4f} - Acc: {acc_score:.4f}")
                # Log loss and accuracy to TensorBoard
                writer.add_scalar("Train Loss", loss.item(), epoch * len(train_loader) + i) #i is the step
                writer.add_scalar("Train Accuracy", acc_score, epoch * len(train_loader) + i) #i is the step
                
                # with torch.backends.cudnn.flags(warn_only=True): 
                loss.backward()
                #for name, param in model.named_parameters():
                    #print(f"Parameter '{name}': requires_grad={param.requires_grad}")
                    #if param.grad is not None and torch.any(param.grad == 0):
                        #print(f"Parameter '{name}' has a zero gradient.")

                
                    


                optimizer.step()
                
                avg_batch_loss = loss.item()
                train_loss += avg_batch_loss * inputs.size(0)

                writer_csv.writerow([idx_fold,'Training',epoch, i, avg_batch_loss, name_patients, labels, output_prob])
            logging.info(f"Epoch: {epoch} - Train loss: {train_loss/len(train_loader)}")
            #logging.info(f"Epoch: {epoch} - Distance of scalar: {distance}")
            
        
            if PARAMETERS['step_save_checkpoints'] is not None:
                logging.info(f"Saving checkpoint for epoch {epoch}")
                if (epoch == PARAMETERS['epoch'] - 1):
                    torch.save(model.state_dict(), os.path.join(PARAMETERS["output_dir"], f"fold-{idx_fold}_epoch-{epoch}.pt"))
                #if epoch % PARAMETERS['step_save_checkpoints'] == 0:                        
                    #torch.save(model.state_dict(), os.path.join(PARAMETERS["output_dir"], f"fold-{idx_fold}_epoch-{epoch}.pt"))

            logging.info(f"Starting Validation Epoch {epoch}")                                                                        
            #Validation
            model.eval()
            
            val_loss = 0.0
            with torch.no_grad():
                for i, (inputs, labels, name_patients) in enumerate(val_loader):
                    inputs, labels = inputs.to(device), labels.to(device)
                    inputs = inputs.type(dtype) #convert to double
            
                    outputs= model(inputs)  

                    outputs = torch.squeeze(outputs,1) #remove the dimension 1   
                    #labels = labels.view(-1, 1)   #att
                    loss = criterion(outputs,labels.float()) 
                    logging.info(f"Epoch [{epoch}/{PARAMETERS['epoch']}] - Step [{i}/{len(train_loader)-1}] - Train Loss: {loss.item():.4f}")
                    
                    labels = labels.detach().cpu().numpy() 
                    output_prob = torch.sigmoid(outputs).detach().cpu().numpy()
                    name_patients = list(name_patients)    
                    
                    preds = np.round(output_prob) #round to 0 or 1
                    acc_score = accuracy_score(y_true = labels, y_pred = preds)                                    
                    
                    # Log loss and accuracy to TensorBoard
                    writer.add_scalar("Val Loss", loss.item(), epoch * len(train_loader) + i) #i is the step
                    writer.add_scalar("Val Acc", acc_score, epoch * len(train_loader) + i) #i is the step
                    
                    avg_batch_loss = loss.item()
                    val_loss += avg_batch_loss * inputs.size(0)
                    
                    writer_csv.writerow([idx_fold,'Val',epoch, i, avg_batch_loss, name_patients, labels, output_prob])
            logging.info(f"Epoch: {epoch} - Val loss: {val_loss/len(val_loader)}")
            #logging.info(f"Epoch: {epoch} - Distance of scalar: {distance}")
            