# Deep Riemannian Representations to quantify Parkinsonian patterns from smooth pursuit eye movement

This repository is related to the paper entitle:
"Deep Riemannian Representations to quantify Parkinsonian patterns from smooth pursuit eye movement" (Under revision). 

Luis Fernando Celis <sup>1</sup> , Juan Olmos <sup>1,2</sup>, Fabio Martínez <sup>1</sup>* Antoine Mazanera <sup>2</sup>

<sup>1</sup>  Biomedical Imaging, Vision and Learning Laboratory (BIVL²ab), Universidad Industrial de Santander (UIS), Bucaramanga 680002,Colombia.

<sup>2</sup> Computer Science and Systems Engineering Laboratory (U2IS), ENSTA Paris, Institut Polytechnique de Paris, 828, Boulevard des
Maréchaux, Palaiseau, 91762, France.


<div align="center">
  <img src="imgs/Pipeline.png" width="100%" height="70%"/>
</div><br/>

# Data
------------

To download the associated imaging data: [Request Access to Dataset](mailto:famarcar@saber.uis.edu.co?cc=jaolmosr@correo.uis.edu.co&subject=Request%20for%20access:%20Ocular%20Fixation%20Dataset). The **Ocular SPEM Dataset** includes 11 patients diagnosed with PD and 11 Control subjects. The study incorporates PD subjects with different disease degree progression (Hoehn-Yahr rating scale). For each patient, the dataset includes all configurations while performing smooth pursuit task.


### Preparing data
This section is currently under construction. Please check back later for updates.


# Contact Information
------------
* Luis Fernando Celis: luis2238327@correo.uis.edu.co

